﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BatMovementHelper.h"
#include "Curves/CurveFloat.h"

DEFINE_LOG_CATEGORY_STATIC(LogBatMovementHelper, All, All)

// Sets default values for this component's properties
UBatMovementHelper::UBatMovementHelper()
{
	// ...
}

float UBatMovementHelper::GetMovementForceForTime(const float Time) const
{
	if (MovementVelocity)
	{
		return MovementVelocityFactor * MovementVelocity->GetFloatValue(Time);
	}

	UE_LOG(LogBatMovementHelper, Error, TEXT("%s No VelocityCurve Set"), *GetName());
	return 0.0f;
}
