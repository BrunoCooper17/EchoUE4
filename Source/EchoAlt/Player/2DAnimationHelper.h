﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "2DAnimationHelper.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class ECHOALT_API U2DAnimationHelper final : public UObject
{
	GENERATED_BODY()
};
