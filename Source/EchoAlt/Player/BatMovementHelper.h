﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "BatMovementHelper.generated.h"


UCLASS(Blueprintable, BlueprintType)
class ECHOALT_API UBatMovementHelper final : public UObject
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBatMovementHelper();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="Movement")
	float GetMovementForceForTime(float Time) const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Config")
	float MovementVelocityFactor = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Config")
	class UCurveFloat* MovementVelocity = nullptr;
};
