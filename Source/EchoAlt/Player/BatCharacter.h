﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "BatCharacter.generated.h"

UCLASS()
class ECHOALT_API ABatCharacter final : public APaperCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABatCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Config")
	class UCurveFloat* MovementForce;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Config")
	TSubclassOf<class AEcholocation> EcholocationClass;
	
	UPROPERTY()
	class AEcholocation* Echolocation;

protected:
	void Move_AxisX(float AxisX);
	void Move_AxisY(float AxisY);	
	void EcholocationAction();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_EcholocationAction(FVector Location);

private:
	float Input_AxisX;
	float Input_AxisY;
};
