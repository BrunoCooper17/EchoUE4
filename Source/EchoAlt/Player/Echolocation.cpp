﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Echolocation.h"

#include "Components/PointLightComponent.h"


// Sets default values
AEcholocation::AEcholocation()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(FName("Root"));

	LightComponent = CreateDefaultSubobject<UPointLightComponent>(FName("EchoLight"));
	LightComponent->bUseInverseSquaredFalloff = false;
	LightComponent->SetCastShadows(false);
	LightComponent->SetIsReplicated(true);
	LightComponent->SetIntensity(1.f);
	LightComponent->SetAttenuationRadius(1.f);
	LightComponent->SetupAttachment(RootComponent);

	bReplicates = true;
	SetReplicatingMovement(true);
}

// Called when the game starts or when spawned
void AEcholocation::BeginPlay()
{
	Super::BeginPlay();
}

void AEcholocation::Server_StartEcholocationAt(const FVector Location)
{
	LightComponent->SetIntensity(0.f);
	LightComponent->SetAttenuationRadius(0.f);
	SetActorHiddenInGame(false);

	SetActorLocation(Location);
	NetMulticast_StartEcholocation();
}

void AEcholocation::NetMulticast_StartEcholocation_Implementation()
{
	StartEcholocation();
}

void AEcholocation::EndEcholocation()
{
	SetActorHiddenInGame(true);
}

// Called every frame
void AEcholocation::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

