﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Echolocation.generated.h"

UCLASS()
class ECHOALT_API AEcholocation final : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AEcholocation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category="Config")
	class UPointLightComponent* LightComponent;

public:
	UFUNCTION(BlueprintImplementableEvent, Category="Echolocation")
	void StartEcholocation();
	
	void Server_StartEcholocationAt(FVector Location);
	UFUNCTION(NetMulticast, Reliable)
    void NetMulticast_StartEcholocation();

protected:
	UFUNCTION(BlueprintCallable, Category="Echolocation")
	void EndEcholocation();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
