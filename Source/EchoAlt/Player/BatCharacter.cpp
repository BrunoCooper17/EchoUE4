﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BatCharacter.h"

#include "Echolocation.h"
#include "PaperFlipbookComponent.h"
#include "Components/InputComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBatPawn, All, All)

// Sets default values
ABatCharacter::ABatCharacter()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABatCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		Echolocation = GetWorld()->SpawnActor<AEcholocation>(EcholocationClass);		
	}
}

// Called every frame
void ABatCharacter::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	// UE_LOG(LogBatPawn, Log, TEXT("Anim Time? %f / %f"), GetSprite()->GetPlaybackPosition(), GetSprite()->GetFlipbookLength());
	if (IsValid(MovementForce))
	{
		const float ForceScale = MovementForce->GetFloatValue(GetSprite()->GetPlaybackPosition());
		AddMovementInput(FVector(Input_AxisX, 0, Input_AxisY), ForceScale);
	}
}

// Called to bind functionality to input
void ABatCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(FName("EchoAction"), IE_Pressed, this, &ABatCharacter::EcholocationAction);
	PlayerInputComponent->BindAxis(FName("Bat_AxisX"), this, &ABatCharacter::Move_AxisX);
	PlayerInputComponent->BindAxis(FName("Bat_AxisY"), this, &ABatCharacter::Move_AxisY);
}

void ABatCharacter::Move_AxisX(const float AxisX)
{
	// UE_LOG(LogBatPawn, Log, TEXT("Axis X: %0.2f"), AxisX)
	Input_AxisX = AxisX;
}

void ABatCharacter::Move_AxisY(const float AxisY)
{
	// UE_LOG(LogBatPawn, Log, TEXT("Axis Y: %0.2f"), AxisY)
	Input_AxisY = AxisY;
}

void ABatCharacter::EcholocationAction()
{
	Server_EcholocationAction(GetActorLocation());
}

void ABatCharacter::Server_EcholocationAction_Implementation(const FVector Location)
{
	if (IsValid(Echolocation))
	{
		Echolocation->Server_StartEcholocationAt(Location);
	}
}

// TODO: Add a good enough validation
bool ABatCharacter::Server_EcholocationAction_Validate(FVector Location)
{
	return true;
}
