// Copyright Epic Games, Inc. All Rights Reserved.

#include "EchoAlt.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EchoAlt, "EchoAlt" );
