// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EchoAltGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ECHOALT_API AEchoAltGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
