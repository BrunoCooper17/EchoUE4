// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "My2DAnimationHelperFactory.generated.h"

/**
 * 
 */
UCLASS()
class ECHOALTEDITORMODULE_API UMy2DAnimationHelperFactory : public UFactory
{
	GENERATED_BODY()

	public:
	UMy2DAnimationHelperFactory();

	// Begin UFactory Interface
	virtual UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;
	// End UFactory Interface
};
