#pragma once

#include "CoreMinimal.h"
#include "AssetTypeActions_Base.h"

/*
*
*/
class FAssetTypeActions_BatMovementHelper final : public FAssetTypeActions_Base
{
public:
	// IAssetTypeActions interface
	virtual FText GetName() const override;
	virtual FColor GetTypeColor() const override;
	virtual UClass* GetSupportedClass() const override;
	virtual uint32 GetCategories() override;
	// End of IAssetTypeActions interface
};
