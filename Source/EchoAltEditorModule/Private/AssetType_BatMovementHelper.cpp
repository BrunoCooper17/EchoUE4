// Fill out your copyright notice in the Description page of Project Settings.


#include "AssetType_BatMovementHelper.h"
#include "EchoAlt/Player/BatMovementHelper.h"

#define LOCTEXT_NAMESPACE "AssetType_BatMovementHelper"

FText FAssetTypeActions_BatMovementHelper::GetName() const
{
	return NSLOCTEXT("AssetType_BatMovementHelper", "AssetType_BatMovementHelper", "BatMovementHelper");
}

FColor FAssetTypeActions_BatMovementHelper::GetTypeColor() const
{
	return FColor::Turquoise;
}

UClass* FAssetTypeActions_BatMovementHelper::GetSupportedClass() const
{
	return UBatMovementHelper::StaticClass();
}

uint32 FAssetTypeActions_BatMovementHelper::GetCategories()
{
	return EAssetTypeCategories::Misc;
}

#undef LOCTEXT_NAMESPACE