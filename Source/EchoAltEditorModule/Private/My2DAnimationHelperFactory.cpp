// Fill out your copyright notice in the Description page of Project Settings.


#include "My2DAnimationHelperFactory.h"
#include "EchoAlt/Player/2DAnimationHelper.h"

UMy2DAnimationHelperFactory::UMy2DAnimationHelperFactory()
{
	// Provide the factory with information about how to handle our asset
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = U2DAnimationHelper::StaticClass();
}

UObject* UMy2DAnimationHelperFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags,
	UObject* Context, FFeedbackContext* Warn)
{
	// Create and return a new instance of our BatMovementHelper object
	U2DAnimationHelper* MyCustomAsset = NewObject<U2DAnimationHelper>(InParent, Class, Name, Flags);
	return MyCustomAsset;
}
