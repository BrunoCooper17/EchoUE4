// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBatMovementHelperFactory.h"
#include "EchoAlt/Player/BatMovementHelper.h"

UMyBatMovementHelperFactory::UMyBatMovementHelperFactory()
{
	// Provide the factory with information about how to handle our asset
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UBatMovementHelper::StaticClass();
}

UObject* UMyBatMovementHelperFactory::FactoryCreateNew(UClass* Class, UObject* InParent, const FName Name,
                                                       const EObjectFlags Flags, UObject* Context,
                                                       FFeedbackContext* Warn)
{
	// Create and return a new instance of our BatMovementHelper object
	UBatMovementHelper* MyCustomAsset = NewObject<UBatMovementHelper>(InParent, Class, Name, Flags);
	return MyCustomAsset;
}
