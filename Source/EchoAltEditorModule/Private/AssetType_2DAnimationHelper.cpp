// Fill out your copyright notice in the Description page of Project Settings.


#include "AssetType_2DAnimationHelper.h"
#include "EchoAlt/Player/2DAnimationHelper.h"

#define LOCTEXT_NAMESPACE "AssetType_2DAnimationHelper"

FText FAssetTypeActions_2DAnimationHelper::GetName() const
{
	return NSLOCTEXT("AssetType_2DAnimationHelper", "AssetType_2DAnimationHelper", "2DAnimationHelper");
}

FColor FAssetTypeActions_2DAnimationHelper::GetTypeColor() const
{
	return FColor::Turquoise;
}

UClass* FAssetTypeActions_2DAnimationHelper::GetSupportedClass() const
{
	return U2DAnimationHelper::StaticClass();
}

uint32 FAssetTypeActions_2DAnimationHelper::GetCategories()
{
	return EAssetTypeCategories::Misc;
}

#undef LOCTEXT_NAMESPACE